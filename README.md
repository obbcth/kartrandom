**카트라이더 트랙 랜덤추첨기, KartRandom입니다.**

빻! 종! 레이스를 위해 만들어진 랜덤 추첨기입니다.

타 스트리머 분들도 자유롭게 사용하실 수 있습니다.

---

## 다운로드

https://bitbucket.org/obbcth/kartrandom/downloads/
최신버전을 받아 압축 해제 후 실행하세요.

---

## 기여하기

C# WPF 프로그래밍을 할 수 있으신 분은 아래의 절차를 따라 프로젝트를 빌드할 수 있습니다. (윈도우 기준)

1. Github Desktop 다운로드
2. Clone New Repository 선택
3. **https://obbcth@bitbucket.org/obbcth/kartrandom.git** 복사 붙여넣기
4. **KartRandom.sln** 프로젝트 파일을 열어 수정사항 반영

수정사항 반영은 PR을 열면 검토 후 프로젝트에 반영하겠습니다.
