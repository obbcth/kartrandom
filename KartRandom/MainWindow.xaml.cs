﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Win32;
using Newtonsoft.Json.Linq;

namespace KartRandom
{
    public partial class MainWindow : Window
    {
        // KartRandom 버전
        string VERSION = "KartRandom v4.0";

        // 맵 언제 플레이했는지 기록하는 변수, <맵이름, 플레이시간> 형식
        Dictionary<string, int> last_map = new Dictionary<string, int>();

        // 룰렛
        DispatcherTimer obj = new DispatcherTimer();
        DispatcherTimer obj2 = new DispatcherTimer();

        int t = 0;
        int h = 0;

        string roulette_spin = Path.GetTempFileName() + ".mp3";
        string spin_stop = Path.GetTempFileName() + ".mp3";
        string real = string.Empty;

        // A, B, C, D 맵 타입 
        string map_type = "a";
        string[] map_list = { "a", "b", "c", "d" , "a", "b", "c", "d" };

        // Dictionary 형식, <맵타입, 리스트> 형식
        Dictionary<string, List<string>> MapList = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> MapList_shuffle = new Dictionary<string, List<string>>();

        // 랜덤 몇번 진행했냐
        int randcount = 0;

        // 맵 불러온 뒤 Tooltip 추가
        public void ShowTooltip()
        {
            string a = "";
            foreach (string i in MapList["a"])
                a = a + "\n" + i;
            A_TYPE.ToolTip = a.Trim();

            string b = "";
            foreach (string i in MapList["b"])
                b = b + "\n" + i;
            B_TYPE.ToolTip = b.Trim();

            string c = "";
            foreach (string i in MapList["c"])
                c = c + "\n" + i;
            C_TYPE.ToolTip = c.Trim();

            string d = "";
            foreach (string i in MapList["d"])
                d = d + "\n" + i;
            D_TYPE.ToolTip = d.Trim();
        }

        // 맵 버전 체크
        public void MapVerCheck()
        {
            try
            {
                WebClient wC = new WebClient();
                wC.Headers.Add("user-agent", "Request");
                var json = wC.DownloadString("https://api.github.com/repos/obbcth/kartrandom/branches/master");
                JObject data = JObject.Parse(json);
                var l = data.SelectTokens("$..author.date").ToList();
                mapupdate.Text = "맵 업데이트 : " + l[0].ToString();
            }
            catch
            {
                mapupdate.Text = "맵 업데이트 날짜 불러오기 실패";
            }
        }

        // 초기 작업 완료 시
        protected void TaskWorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            select_button.IsEnabled = true;
            open_button.IsEnabled = true;
            setting_button.IsEnabled = true;
            select_button.Content = "추첨";
            select_button.FontSize = 24;
            ShowTooltip();
            MapVerCheck();
        }

        // 초기 작업중 (DB 받은 후 불러오기)
        public void Worker(object sender, DoWorkEventArgs e)
        {
            // DB파일 다운로드 시 생성되는 TEMP파일
            string tmpfile = Path.GetTempFileName();

            try
            {
                WebClient wC = new WebClient();

                try
                {
                    wC.DownloadFile("https://github.com/obbcth/KartRandom/blob/master/Kart.db?raw=true", tmpfile);
                }
                catch (WebException)
                {
                    wC.DownloadFile("https://bitbucket.org/obbcth/kartrandom/raw/master/src/Kart.db", tmpfile);
                }

                // SQLite.Interop.dll 파일을 exe 안에 임베딩
                // 실행 시 x86 폴더 생성, 그 안에 dll 집어넣어 exe 단독 실행 가능케 하기
                string fileName = Environment.CurrentDirectory + @"\x86\SQLite.Interop.dll";
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));

                Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("KartRandom.SQLite.Interop.dll");
                FileStream fileStream = new FileStream(fileName, FileMode.Create);
                for (int i = 0; i < stream.Length; i++)
                    fileStream.WriteByte((byte)stream.ReadByte());
                fileStream.Close();
            }
            catch
            {
                MessageBox.Show("온라인에서 DB정보를 받아오는데 실패했습니다.\n\nCSV를 선택하여 오프라인으로 진행하세요.");
            }

            try
            {
                string connString = @"Data Source = " + tmpfile + ";";
                using (SQLiteConnection conn = new SQLiteConnection(connString))
                {
                    conn.Open();
                    string sql = "select * from MAP";
                    SQLiteCommand command = new SQLiteCommand(sql, conn);
                    SQLiteDataReader rdr = command.ExecuteReader();

                    // 맵 전부 불러와서, X_TYPE 에 1 체크되어 있으면 해당되는 항목에 추가
                    while (rdr.Read())
                    {
                        if (rdr["A_TYPE"].ToString() == "1")
                            MapList["a"].Add(rdr["NAME"].ToString());

                        if (rdr["B_TYPE"].ToString() == "1")
                            MapList["b"].Add(rdr["NAME"].ToString());

                        if (rdr["C_TYPE"].ToString() == "1")
                            MapList["c"].Add(rdr["NAME"].ToString());

                        if (rdr["D_TYPE"].ToString() == "1")
                            MapList["d"].Add(rdr["NAME"].ToString());
                    }
                    rdr.Close();
                    command.Dispose();
                    conn.Close();
                }
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("맵 분류 과정에서 문제 발생 (온라인)");
            }

            try
            {
                // 룰렛 mp3 다운로드
                WebClient wC = new WebClient();
                try
                {
                    wC.DownloadFile("https://github.com/obbcth/KartRandom/blob/master/auto_roulette.mp3?raw=true", roulette_spin);
                    wC.DownloadFile("https://github.com/obbcth/KartRandom/blob/master/roulette_spinstop.mp3?raw=true", spin_stop);
                }
                catch (WebException)
                {
                    wC.DownloadFile("https://bitbucket.org/obbcth/kartrandom/raw/master/src/auto_roulette.mp3", roulette_spin);
                    wC.DownloadFile("https://bitbucket.org/obbcth/kartrandom/raw/master/src/roulette_spinstop.mp3", spin_stop);
                }
            }
            catch
            {
                MessageBox.Show("룰렛 mp3 음원받기 실패");
            }

        }

        // 설정 파일 경로 설정, %APPDATA% 내부에 KartRandom 폴더 생성
        readonly string settings_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\KartRandom\\";

        // 설정 파일 불러오기, 없으면 생성
        public void loading()
        {            
            // 설정 파일은 KartRandom 폴더 안 setting.txt로 저장
            DirectoryInfo di = new DirectoryInfo(settings_path);
            if (!di.Exists) di.Create();

            if (!File.Exists(settings_path + "setting.txt"))
            {
                MessageBox.Show("환영합니다! KartRandom 빻종레이스 추첨기입니다.\n사용법은 동호형 팬카페에 게시되어 있습니다.\n\n각각의 맵 타입은 마우스 커서를 얹으면 보입니다.\n우측 폴더 아이콘은 오프라인 맵파일을 불러올 수 있습니다.\n우측 설정 아이콘은 각종 기능을 키고 끌 수 있습니다.\n\n문의는 카페글의 댓글로 받습니다.\n타 스트리머분들 자유롭게 쓰셔도 상관없습니다.");

                StreamWriter create = File.CreateText(settings_path + "setting.txt");
                create.WriteLine("False"); // 딜레이 1초
                create.WriteLine("True"); // TTS 읽어주기
                create.WriteLine("False"); // 크기 2배
                create.WriteLine("True"); // True : 넥슨 랜덤, False : 오리지널 랜덤
                create.WriteLine("True"); // TTS 애니메이션
                create.WriteLine("kakao"); // TTS 종류
                create.Dispose();
            }
                
            string[] lines = File.ReadAllLines(settings_path + "setting.txt");

            try
            {
                if (Convert.ToBoolean(lines[0])) delay.Text = "1";
                if (Convert.ToBoolean(lines[1])) tts.Text = "1";
                if (Convert.ToBoolean(lines[2])) old.Text = "1";
                if (Convert.ToBoolean(lines[3])) randtype.Text = "nexon"; else randtype.Text = "original";
                if (Convert.ToBoolean(lines[4])) animation.Text = "1";
                if (lines[5] == "") throw new ArgumentException("5");
                tts_type.Text = lines[5];

                if (Convert.ToBoolean(lines[2]))
                {
                    Application.Current.MainWindow.Height = 280;
                    Application.Current.MainWindow.Width = 900;

                    A_TYPE.FontSize = 24;
                    B_TYPE.FontSize = 24;
                    C_TYPE.FontSize = 24;
                    D_TYPE.FontSize = 24;

                    select_button.FontSize = 48;
                    name.FontSize = 40;

                    open_img.Height = 30;
                    open_img.Width = 30;
                    setting_img.Height = 30;
                    setting_img.Width = 30;
                }
            }

            catch
            {
                MessageBox.Show(VERSION + "로 업데이트가 되었습니다.\n\n새로운 TTS 목소리와 룰렛 애니메이션을 만나보세요!");

                StreamWriter create = File.CreateText(settings_path + "setting.txt");
                create.WriteLine("False");
                create.WriteLine("True");
                create.WriteLine("False");
                create.WriteLine("True");
                create.WriteLine("True");
                create.WriteLine("kakao");
                create.Dispose();
            }

            // 한번 키를 싹 넣어주어야 오류가 생기지 않음
            foreach (int i in Enumerable.Range(0,4))
                MapList[map_list[i]] = new List<string>();
        }

        // 메인 윈도우
        public MainWindow()
        {
            InitializeComponent();
            loading();

            version.Text = VERSION;
            Title = version.Text;
            
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += Worker;
            bw.RunWorkerCompleted += TaskWorkComplete;
            bw.RunWorkerAsync();
        }

        // 폴더 버튼 클릭 시
        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            {
                of.Filter = "CSV 파일 | *.csv";
                of.CheckFileExists = true;
                of.CheckPathExists = true;
            }
            of.ShowDialog();

            try
            {
                StreamReader sr = new StreamReader(of.FileName, Encoding.Default);

                // 키 백업, 한번 키를 싹 넣어주어야 오류가 생기지 않음
                foreach (int i in Enumerable.Range(0, 4))
                    MapList_shuffle[map_list[i]] = MapList[map_list[i]];

                foreach (int i in Enumerable.Range(0, 4))
                    MapList[map_list[i]] = new List<string>();

                while (!sr.EndOfStream)
                {
                    string[] temp = sr.ReadLine().Split(',');
                    foreach (int i in Enumerable.Range(4, 4))
                        if (temp[i].ToString() == "1")
                            MapList[map_list[i]].Add(temp[0].ToString());
                }
                Title = version.Text + " (오프라인 모드)";
                ShowTooltip();
                MessageBox.Show("성공적으로 맵을 불러왔습니다.");
            }
            catch (ArgumentException) { }
            catch
            {
                foreach (int i in Enumerable.Range(0, 4))
                    MapList[map_list[i]] = MapList_shuffle[map_list[i]];
                randcount = 0;
                MessageBox.Show("제대로 된 파일을 불러왔는지 확인하세요.");
            }
        }

        // 설정 버튼 클릭 시
        private void Setting_Click(object sender, RoutedEventArgs e)
        {
            Setting setting = new Setting();
            setting.Show();
        }

        // 추첨 버튼 클릭 시
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Random rnd = new Random();
                if (randtype.Text == "nexon")
                {
                    if (randcount == 0 || randcount >= MapList_shuffle[map_type].Count)
                    {
                        MapList_shuffle[map_type] = MapList[map_type].OrderBy(a => rnd.Next()).ToList();
                        randcount = 0;
                    }
                    name.Text = MapList_shuffle[map_type][randcount++];
                }
                else name.Text = MapList_shuffle[map_type][rnd.Next(MapList_shuffle[map_type].Count)];
            }
            catch
            {
                name.Text = "(설정한 맵 없음)";
            }

            // 애니메이션
            // TTS랑 순서 겹치치 않게 빼버리기

            if (name.Text != "(설정한 맵 없음)")
            {
                if (animation.Text == "1")
                {
                    real = name.Text;
                    AnimationStart();
                }
                else
                {
                    // 만약 TTS 활성화했다면
                    if (tts.Text == "1")
                        ttsread();

                    // 만약 딜레이를 주었다면
                    if (delay.Text == "1")
                    {
                        select_button.IsEnabled = false;
                        BackgroundWorker bw2 = new BackgroundWorker();
                        bw2.DoWork += Wait;
                        bw2.RunWorkerCompleted += WaitComplete;
                        bw2.RunWorkerAsync();
                    }
                }
            }

            // 현재 시간 Unix 타임스탬프 형식으로 저장

            int unixTimestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

            // 마지막으로 플레이 한 시간 불러오기

            if (last_map.ContainsKey(name.Text)) // 만약 이전에 플레이 한 기록이 있다면
            {
                // 글자 표시처리
                last_playtime.Visibility = Visibility.Visible;

                int timeDifference = unixTimestamp - last_map[name.Text];
                // 초 단위로 구분

                // 만약 timeDifference가 60초 이상일 경우 60으로 나눈 몫을 분으로 설정
                // 만약 timeDifference가 3600초 이상일 경우 3600으로 나눈 몪을 시간, 60으로 나눈 몫을 분으로 설정

                if (timeDifference < 60) // 초 단위로 표시
                {
                    last_playtime.Text = "마지막으로 플레이 한 시간 : " + timeDifference + "초 전";
                }
                else
                {
                    if (timeDifference >= 3600)
                    {
                        last_playtime.Text = "마지막으로 플레이 한 시간 : " + timeDifference / 3600 + "시간 " + timeDifference / 60 + "분 전";
                    }
                    else
                    {
                        last_playtime.Text = "마지막으로 플레이 한 시간 : " + timeDifference / 60 + "분 전";
                    }
                }
                
            }

            else
            {
                // 이전에 플레이 한 기록이 없다면 글자 숨김표시
                last_playtime.Visibility = Visibility.Hidden;
            }

            // 마지막으로 플레이 한 시간 기록하기

            last_map[name.Text] = unixTimestamp;


        }

        // 룰렛 애니메이션
        public void AnimationStart()
        {
            select_button.IsEnabled = false;
            A_TYPE.IsEnabled = false;
            B_TYPE.IsEnabled = false;
            C_TYPE.IsEnabled = false;
            D_TYPE.IsEnabled = false;

            mMediaplayer.Open(new Uri(roulette_spin, UriKind.RelativeOrAbsolute));
            mMediaplayer.Play();

            try
            {
                obj.Interval = new TimeSpan(0, 0, 0, 0, 300);
                obj.Start();
                obj.Tick += Obj_Tick;
            }
            catch
            { 
                select_button.IsEnabled = true;
                A_TYPE.IsEnabled = true;
                B_TYPE.IsEnabled = true;
                C_TYPE.IsEnabled = true;
                D_TYPE.IsEnabled = true;
            }

        }

        private void Obj_Tick(object sender, EventArgs e)
        {
            try
            {
                if (t >= MapList_shuffle[map_type].Count()) t = 0;
                name.Text = MapList_shuffle[map_type][t++];
                obj.Stop();

                obj2.Interval = new TimeSpan(0, 0, 0, 0, 300);
                obj2.Start();
                obj2.Tick += Obj2_Tick;
            }
            catch
            {
                select_button.IsEnabled = true;
                A_TYPE.IsEnabled = true;
                B_TYPE.IsEnabled = true;
                C_TYPE.IsEnabled = true;
                D_TYPE.IsEnabled = true;
            }
        }

        private void Obj2_Tick(object sender, EventArgs e)
        {
            try
            {
                h++;

                if (t >= MapList_shuffle[map_type].Count()) t = 0;
                name.Text = MapList_shuffle[map_type][t++];
                obj2.Stop();

                if (h <= 45)
                {
                    obj2 = new DispatcherTimer();
                    obj2.Interval = new TimeSpan(0, 0, 0, 0, 50);
                    obj2.Start();
                    obj2.Tick += Obj2_Tick;
                }
                else if (h <= 50)
                {
                    obj2 = new DispatcherTimer();
                    obj2.Interval = new TimeSpan(0, 0, 0, 0, 150);
                    obj2.Start();
                    obj2.Tick += Obj2_Tick;
                }
                else if (h <= 53)
                {
                    obj2 = new DispatcherTimer();
                    obj2.Interval = new TimeSpan(0, 0, 0, 0, 350);
                    obj2.Start();
                    obj2.Tick += Obj2_Tick;
                }
                else
                {
                    name.Text = real;
                    h = 0;

                    select_button.IsEnabled = true;
                    A_TYPE.IsEnabled = true;
                    B_TYPE.IsEnabled = true;
                    C_TYPE.IsEnabled = true;
                    D_TYPE.IsEnabled = true;


                    // 만약 TTS 활성화했다면
                    if (tts.Text == "1")
                        ttsread();
                    else
                    {
                        mMediaplayer.Open(new Uri(spin_stop, UriKind.RelativeOrAbsolute));
                        mMediaplayer.Play();
                    }
                }
            }
            catch
            {
                select_button.IsEnabled = true;
                A_TYPE.IsEnabled = true;
                B_TYPE.IsEnabled = true;
                C_TYPE.IsEnabled = true;
                D_TYPE.IsEnabled = true;
            }
        }

        // 딜레이 1초
        public void Wait(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(1000);
        }

        protected void WaitComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            select_button.IsEnabled = true;
        }

        // TTS 읽기
        private MediaPlayer mMediaplayer = new MediaPlayer();

        public void ttsread()
        {
            try
            {
                string tmpmp3 = Path.GetTempFileName() + ".mp3";

                // kakao TTS 사용
                if (tts_type.Text == "kakao")
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://kakaoi-newtone-openapi.kakao.com/v1/synthesize");
                    httpWebRequest.ContentType = "application/xml";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Headers.Add("Authorization", "KakaoAK 49389386fd3728a190726afa46231fd8");

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string data = "<speak>" + name.Text + "</speak>";
                        streamWriter.Write(data);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var buff = new byte[1024];
                        int pos = 0;
                        int count;

                        using (Stream stream = httpResponse.GetResponseStream())
                        {
                            using (var fs = new FileStream(tmpmp3, FileMode.Create))
                            {
                                do
                                {
                                    count = stream.Read(buff, pos, buff.Length);
                                    fs.Write(buff, 0, count);
                                } while (count > 0);
                            }
                        }
                    }
                }

                // twip (google) TTS 사용
                if (tts_type.Text == "twip")
                {
                    WebClient wC = new WebClient();
                    string url1 = "https://www.google.com/speech-api/v1/synthesize?text=";
                    string url2 = "&lang=ko-kr&speed=0.5";
                    string strEncode = HttpUtility.UrlEncode(name.Text);
                    wC.DownloadFile(url1 + strEncode + url2, tmpmp3);
                }

                mMediaplayer.Open(new Uri(tmpmp3, UriKind.RelativeOrAbsolute));
                mMediaplayer.Play();
            }
            catch
            {
                name.Text = name.Text + " (TTS 오류)";
            }
        }

        private void A_TYPE_Checked(object sender, RoutedEventArgs e)
        {
            map_type = "a";
            randcount = 0;
        }

        private void B_TYPE_Checked(object sender, RoutedEventArgs e)
        {
            map_type = "b";
            randcount = 0;
        }

        private void C_TYPE_Checked(object sender, RoutedEventArgs e)
        {
            map_type = "c";
            randcount = 0;
        }

        private void D_TYPE_Checked(object sender, RoutedEventArgs e)
        {
            map_type = "d";
            randcount = 0;
        }
    }
}
