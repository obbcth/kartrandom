﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;

namespace KartRandom
{
    public partial class Setting : Window
    {
        readonly string settings_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\KartRandom\\";

        public Setting()
        {
            InitializeComponent();

            DirectoryInfo di = new DirectoryInfo(settings_path);
            if (!di.Exists) di.Create();

            string[] lines = File.ReadAllLines(settings_path + "setting.txt");
            
            Delay.IsChecked = Convert.ToBoolean(lines[0]);
            TTS.IsChecked = Convert.ToBoolean(lines[1]);
            Old.IsChecked = Convert.ToBoolean(lines[2]);
            Animation.IsChecked = Convert.ToBoolean(lines[4]);
            if (Convert.ToBoolean(lines[3])) Nexon.IsChecked = true; else Original.IsChecked = true;
            version.Text = ((MainWindow)Application.Current.MainWindow).version.Text;
            update.Text = ((MainWindow)Application.Current.MainWindow).mapupdate.Text;

            TTS_Type.Text = ((MainWindow)Application.Current.MainWindow).tts_type.Text;
        }

        private void Nexon_Checked(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).randtype.Text = "nexon";
            save();
        }

        private void Original_Checked(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).randtype.Text = "original";
            save();
        }

        private void Delay_Click(object sender, RoutedEventArgs e)
        {
            if (Delay.IsChecked == true)
            {
                ((MainWindow)Application.Current.MainWindow).delay.Text = "1";
            }
            else
            {
                ((MainWindow)Application.Current.MainWindow).delay.Text = "0";
            }
            save();
        }

        private void Old_Click(object sender, RoutedEventArgs e)
        {
            if (Old.IsChecked == true)
            {
                ((MainWindow)Application.Current.MainWindow).old.Text = "1";

                Application.Current.MainWindow.Height = 280;
                Application.Current.MainWindow.Width = 900;

                ((MainWindow)Application.Current.MainWindow).A_TYPE.FontSize = 24;
                ((MainWindow)Application.Current.MainWindow).B_TYPE.FontSize = 24;
                ((MainWindow)Application.Current.MainWindow).C_TYPE.FontSize = 24;
                ((MainWindow)Application.Current.MainWindow).D_TYPE.FontSize = 24;

                ((MainWindow)Application.Current.MainWindow).select_button.FontSize = 48;
                ((MainWindow)Application.Current.MainWindow).name.FontSize = 40;
                ((MainWindow)Application.Current.MainWindow).last_playtime.FontSize = 22;

                ((MainWindow)Application.Current.MainWindow).open_img.Height = 30;
                ((MainWindow)Application.Current.MainWindow).open_img.Width = 30;
                ((MainWindow)Application.Current.MainWindow).setting_img.Height = 30;
                ((MainWindow)Application.Current.MainWindow).setting_img.Width = 30;

            }

            else
            {
                ((MainWindow)Application.Current.MainWindow).old.Text = "0";

                Application.Current.MainWindow.Height = 140;
                Application.Current.MainWindow.Width = 450;

                ((MainWindow)Application.Current.MainWindow).A_TYPE.FontSize = 12;
                ((MainWindow)Application.Current.MainWindow).B_TYPE.FontSize = 12;
                ((MainWindow)Application.Current.MainWindow).C_TYPE.FontSize = 12;
                ((MainWindow)Application.Current.MainWindow).D_TYPE.FontSize = 12;

                ((MainWindow)Application.Current.MainWindow).select_button.FontSize = 24;
                ((MainWindow)Application.Current.MainWindow).name.FontSize = 20;
                ((MainWindow)Application.Current.MainWindow).last_playtime.FontSize = 11;

                ((MainWindow)Application.Current.MainWindow).open_img.Height = 15;
                ((MainWindow)Application.Current.MainWindow).open_img.Width = 15;
                ((MainWindow)Application.Current.MainWindow).setting_img.Height = 15;
                ((MainWindow)Application.Current.MainWindow).setting_img.Width = 15;
            }

            save();

        }

        private void TTS_Click(object sender, RoutedEventArgs e)
        {
            if (TTS.IsChecked == true)
            {
                ((MainWindow)Application.Current.MainWindow).tts.Text = "1";
            }
            else
            {
                ((MainWindow)Application.Current.MainWindow).tts.Text = "0";
            }

            save();

        }

        private void Animation_Click(object sender, RoutedEventArgs e)
        {
            if (Animation.IsChecked == true)
            {
                ((MainWindow)Application.Current.MainWindow).animation.Text = "1";
            }
            else
            {
                ((MainWindow)Application.Current.MainWindow).animation.Text = "0";
            }

            save();

        }

        DispatcherTimer obj = new DispatcherTimer();

        private void TTS_Type_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // 임시 땜빵용
            obj = new DispatcherTimer();
            obj.Interval = new TimeSpan(0, 0, 0, 0, 100);
            obj.Start();
            obj.Tick += Obj_Tick;
        }

        private void Obj_Tick(object sender, EventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).tts_type.Text = TTS_Type.Text;
            save();

            obj.Stop();
        }

        public void save()
        {
            string[] lines = { Delay.IsChecked.ToString(), TTS.IsChecked.ToString(), Old.IsChecked.ToString(), Nexon.IsChecked.ToString(), Animation.IsChecked.ToString(), TTS_Type.Text };

            using (StreamWriter sw = new StreamWriter(new FileStream(settings_path + "setting.txt", FileMode.OpenOrCreate)))
            {
                foreach (string line in lines)
                {
                    sw.WriteLine(line);
                }
            }
        }

        private void changelog_Click(object sender, RoutedEventArgs e)
        {
            string result = string.Empty;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("KartRandom.changelog.txt"))
            using (StreamReader rdr = new StreamReader(stream))
                result = rdr.ReadToEnd();

            MessageBox.Show(result);
        }
    }
}
